﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerSlow : MonoBehaviour
{
    public PlayerCtrl player;
    
    public SlowMoCtrl slowMo;

    void Start()
    {
        player = GameObject.Find("Player").GetComponent<PlayerCtrl>();
    }

    void OnTriggerEnter(Collider coll) 
    {
        if (coll.gameObject.name == "Player") 
        {
            slowMo.slowMotion();
            player.AddScore(64);
        }
    }
}
