﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMoCtrl : MonoBehaviour
{

    public float slowMoValue = 0.07f;
    public float slowMoDuration = 5f;
 

    void Update() 
    {
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0.0f, 1.0f);
        Time.fixedDeltaTime = Mathf.Clamp(Time.fixedDeltaTime, 0.0f, 1.0f);
        Time.timeScale += (1f / slowMoDuration) * Time.unscaledDeltaTime;
    }

    public void slowMotion() 
    {
        Time.timeScale = slowMoValue;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
    
}
