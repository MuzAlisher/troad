﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuPause : MonoBehaviour
{
    public GameObject PAUSE;
    

    public void Restart() 
    {
        SceneManager.LoadScene(1);
        
    }
    public void Exit()
    {
        Application.LoadLevel(0);
    }
}
