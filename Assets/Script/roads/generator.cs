﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generator : MonoBehaviour
{
    public bool isSameDelay;//одинаковое время спавна
 
    public bool isRandomObj;//если это рандрный объект
    
    public GameObject ObjToSpawn;
    
    public GameObject[] ObjsToSpawn;

    public float timeBeforSpawn;//время перед тем как начнется спавн

    public float spawnDelay;

    public int minDelay;//Время между возрождениями

    public int maxDelay; 
    
    void Start()
    {

        if (isSameDelay == true)
        {
            InvokeRepeating("Spawn", timeBeforSpawn, spawnDelay);
        }
        if (isSameDelay == false)
        {
            StartCoroutine(Spawner());
        }
    }

    IEnumerator Spawner() 
    {
        yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
        Spawn();
    }
    
    void Spawn() 
    {
       if(isRandomObj==true){
        GameObject obj = Instantiate(ObjsToSpawn[Random.Range(0, ObjsToSpawn.Length)], transform.position, transform.rotation) as GameObject;
       
       }
       if (isRandomObj == false)
       {
         
           GameObject obj = Instantiate(ObjToSpawn, transform.position, transform.rotation) as GameObject;

       }
        
       if (isSameDelay == false) 
       {
           StartCoroutine(Spawner());
       }
    }
    /*void Start () {
		if(isSameDelay)
		{
			InvokeRepeating("Spawn",timeBeforSpawn,spawnDelay);
		}
		else
		{
		StartCoroutine(Spawner());
		}
	}
	
	IEnumerator Spawner()
	{
		yield return new WaitForSeconds(Random.Range(MinDelay,MaxDelay));
		Spawn();
	}
*/

 
}
