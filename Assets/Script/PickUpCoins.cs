﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpCoins : MonoBehaviour
{

    public Text coinsCount;

    void Start() 
    {
        coinsCount = GameObject.Find("CoinsCount").GetComponent<Text>();
        //sm = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

    void OnTriggerEnter(Collider coinColl) 
    {
        if(coinColl.name == "Player")
        {
            coinsCount.text = (int.Parse(coinsCount.text)+1).ToString();
            Destroy(gameObject);    
        }
    }

}
