﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCtrl : MonoBehaviour
{
    public GameObject player;

    public Transform target;

    public int Life_point;

    public bool isDead = false;

    public bool enter;

    public bool pause;

    public GameObject pausePanel;

    public float Speed;

    public Vector3 targetMoveLeft;

    public Vector3 targetMoveRight;

    public int scoreCount;

    public Text scoreCountT;

    public SlowMoCtrl SlowMo;

    public bool isRight;

    public bool isLeft;



    void Update()
    {
        scoreCountT.text = scoreCount.ToString();
        //Vector3 movePos = new Vector3(-6f, target.position.y, target.position.z);
         Vector3 movePos = new Vector3(target.position.x, target.position.y,-15f);
        
        if (!isDead)
        {         

            
            if (isRight)
            {
                if (target.transform.position.x < 2.3f)
                {
                    target.transform.Translate(targetMoveRight * Speed * Time.deltaTime);
                }
            }
            if (isLeft)
            {
                if (target.transform.position.x > -2.3f)
                {                   
                    target.transform.Translate(targetMoveLeft * Speed * Time.deltaTime);
                }
            }
            transform.LookAt(target);
            transform.position = Vector3.MoveTowards(transform.position, movePos, Time.deltaTime * (Speed - 0.5f));
        }
    }

    

    void Boom() 
    {
        isDead= true;
        Destroy(player);
        PauseM();
    }
//
    public void PauseM()
    {
        if (pause == true)
        {
            pausePanel.SetActive(false);
            Time.timeScale = 1f;
            pause = false;
            return;
        }
        if (pause == false)
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
            pause = true;
            return;
        }
    }

    
    void OnCollisionEnter(Collision coll) 
    {
        if (coll.gameObject.CompareTag("car")) 
        {
            Damage(1);
            if (Life_point == 0 && !isDead)
            {
                Boom();
            }
        }
    }

    void Damage(int dmg) 
    {
        if (Life_point > 1)
        {
            Life_point -= dmg;
        }
        else 
        {
            Boom();
        }
    }

    public void AddScore(int addScore)
    {
        scoreCount += addScore;
    }


    public void TurnLeft(bool isOn)
    {
        isLeft = isOn;
    }
    public void TurnRight(bool isOn) 
    {
        isRight = isOn;
    }
 
}
