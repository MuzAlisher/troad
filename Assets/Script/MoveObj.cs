﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObj : MonoBehaviour
{
    public bool isCar = false;

    public float MinSpeed;
    public float MaxSpeed;    

    public float speed;

    public Transform otherCar;

    public Vector3 dir;//направление

    void Start()
    {
        Destroy(gameObject, 19);
        
    }

    void Update()
    {
      transform.Translate(dir * speed * Time.deltaTime, Space.World); //движение Изменение transform.translate((x,y,z),)
      if (isCar) 
      {
          RandomSpeed();
      }
    }

   
    void OnCollisionEnter(Collision coll) //Destroy машин
    {
        if (coll.gameObject.CompareTag("car"))
        {
            Destroy(gameObject);
        }
    }

    void RandomSpeed()
    {
        transform.Translate(dir * Random.Range(MinSpeed,MaxSpeed)* Time.deltaTime, Space.World);
    }


}
