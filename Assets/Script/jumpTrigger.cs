﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpTrigger : MonoBehaviour
{
    public Rigidbody player;
    public int jumpImpulse;
    public bool isReadyToJump;
    public float jumpDelay; //время между прыжками
    public bool isJump;

    void Start() 
    {
        isReadyToJump = true;
    }


    void OnTriggerStay(Collider coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            if (isJump && isReadyToJump)
            {
                Jump();
            }
        }
    }
    void Jump() 
    {
        
        player.AddForce(Vector3.up * jumpImpulse ,ForceMode.Impulse);
        print("jump lol");
        isReadyToJump = false;
        if (!isReadyToJump) 
        {
            StartCoroutine(JumpDelay()); 
        }
    }
        
    IEnumerator JumpDelay() 
    {
        yield return new WaitForSeconds(jumpDelay);
        isReadyToJump = true;
    }
    
    public void Jumping(bool _isOn) 
    {
        isJump = _isOn;
    }

}
